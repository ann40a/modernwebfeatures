CSS Variables  
- [article](https://webdesign.tutsplus.com/tutorials/the-new-css--cms-28888) about new native css features
- [article](https://developers.google.com/web/updates/2016/02/css-variables-why-should-you-care) about css variables from developers.google.com  
- [article](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_variables) about css variables from developer.mozilla.org  
