import {instance as themesService} from './ThemesService.js';

class EventHandlers {
    static onThemeChange(value) {
        themesService.selectTheme(value);
    }
}

export default EventHandlers;
