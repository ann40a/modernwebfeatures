class Theme {
    constructor(primaryColor, secondaryColor) {
        this.primaryColor = primaryColor;
        this.secondaryColor = secondaryColor;
    }
}

class ThemesService {
    constructor() {
        this.themes = {
            red: new Theme('rgb(255, 0, 0)', 'rgb(250, 220, 220)'),
            green: new Theme('rgb(0, 100, 0)', 'rgb(200, 250, 200)'),
            blue: new Theme('rgb(0, 0, 255)', 'rgb(220, 230, 240)'),
        };
    }

    selectTheme(name) {
        const theme = this.themes[name];
        const rootStyle = document.documentElement.style;
        rootStyle.setProperty('--primary-color', theme.primaryColor);
        rootStyle.setProperty('--secondary-color', theme.secondaryColor);
    }
}

export const instance = new ThemesService();
